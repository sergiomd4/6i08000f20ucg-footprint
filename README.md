# 6I08000F20UCG footprint

This is the footprint in KiCad of a crystal SMD5032-2P, whose MFR Part# is 6I08000F20UCG. The manufacturer is sjk (shenzhen Crystal tech).

## Characteristics
* Frequency         8 MHz
* Load capacitance  20 pF